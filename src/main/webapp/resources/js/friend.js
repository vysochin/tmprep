/**
 * Created by admin on 11.04.2015.
 */

$(document).ready(function () {

    $('.addFriend').on('click',addFriend);

    function addFriend(e) {
        e.preventDefault();
        var reqData = {
            userId: $(this).data('id')
        };

        $.ajax('/user/friend/add', {
            type: 'POST',
            data: reqData
        }).done(function (response) {
            $('a').remove('.addFriend');
            $('a').remove('.blackListRemove');
            $('a').remove('.blackListAdd');
            $('<p class="info">You sent the invitation</p>').prependTo('.profileMenu');
            $('<a href="/" class="blackListAdd boldRef" data-id="'+reqData.userId+'">Add to Black List</a>').appendTo('.profileMenu').on('click', blackListAdd);
        });

    }

    $('.deleteFriend').on('click', deleteFriend);
        function deleteFriend(e) {

        e.preventDefault();
        var reqData = {
            userId: $(this).data('id')
        };

        $.ajax('/user/friend/delete', {
            type: 'POST',
            data: reqData
        }).done(function () {
            $('a').remove('.confirm');
            $('a').remove('.deleteFriend');
            $('a').remove('.message');
            $('<a href="/" class="addFriend boldRef" data-id="'+reqData.userId+'">Add friend</a>').prependTo('.profileMenu').on('click', addFriend);
            $('p').remove('.info');
        });

    }

    $('.confirm').on('click', confirm );
        function confirm(e) {
        e.preventDefault();
        var reqData = {
            userId: $(this).data('id')
        };
        $.ajax('/user/friend/confirm', {
            type: 'POST',
            data: reqData
        }).done(function (response) {
            var user = JSON.parse(response);
            $('a').remove('.confirm');
            $('a').remove('.deleteFriend');


            $('<a href="/" class="deleteFriend boldRef" data-id="'+reqData.userId+'">Delete friend</a>').prependTo('.profileMenu').on('click', deleteFriend);
            $('<a href="/user/message/'+ user.uri+'" class="message">Send message</a>').prependTo('.profileMenu');
            $('<p class="info">'+user.name+' your Friend</p>').prependTo('.profileMenu');

        });
    }

    $('.blackListAdd').on('click', blackListAdd);
        function blackListAdd(e) {
        e.preventDefault();
        var reqData = {
            userId: $(this).data('id')
        };
        $.ajax('/user/blackList/add', {
            type: 'POST',
            data: reqData
        }).done(function () {
            $('a').remove('.addFriend');
            $('a').remove('.blackListAdd');
            $('p').remove('.info');
            $('a').remove('.confirm');
            $('a').remove('.deleteFriend');
            $('a').remove('.message');
            $('<a href="/" class="addFriend boldRef" data-id="'+reqData.userId+'">Add friend</a>').prependTo('.profileMenu').on('click', addFriend);
            $('<a href="/" class="blackListRemove boldRef" data-id="'+reqData.userId+'">Remove from Black List</a>').appendTo('.profileMenu').on('click', blackListRemove);
        });
    }

    $('.blackListRemove').on('click', blackListRemove);
    function blackListRemove(e) {
        e.preventDefault();
        var reqData = {
            userId: $(this).data('id')
        };
        $.ajax('/user/blackList/remove', {
            type: 'POST',
            data: reqData
        }).done(function () {
            $('a').remove('.blackListRemove');
            $('<a href="/" class="blackListAdd boldRef" data-id="'+reqData.userId+'">Add to Black List</a>').appendTo('.profileMenu').on('click', blackListAdd);
        });
    }
});