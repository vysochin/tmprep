/**
 * Created by admin on 07.04.2015.
 */
$(document).ready(function () {
    $('.js-submit').on('click', function (e) {
        e.preventDefault();
        var $text = $('.js-text');
        if (!($text.val() == '')) {
            var reqData = {
                text: $text.val(),
                toUserId: $text.data('id')
            };
            $.ajax('/user/profile/addWallPost', {
                type: 'POST',
                data: reqData
            }).done(function (response) {
                var post = JSON.parse(response);
                $('<div></div>', {
                    class: 'post' + post.id
                }).prependTo('.wall');
                $('<hr>').appendTo('.' + 'post' + post.id);
                $('<img>', {
                    src: '/resources/images/avatars/' + post.avatarNameRecipient,
                    width: '30',
                    height: '30'
                }).appendTo('.' + 'post' + post.id);
                $('<a href=\"/user/profile/' + post.uriRecipient + '\" class=\"boldRef">'
                + ' ' + post.firstNameRecipient + ' ' + (post.lastNameRecipient ? post.lastNameRecipient : '')
                + ' </a>').appendTo('.' + 'post' + post.id);
                var dateObj = new Date(post.date);
                $('<span class="date">'
                + $.format.date(
                    dateObj, "dd.MM.yyyy HH:mm:ss") + '</span>').appendTo('.' + 'post' + post.id);
                $('<p>' + post.text + '</p>').appendTo('.' + 'post' + post.id);
                var $deletePostLink = $('<a href="/user/profile/deleteWallPost" class="del" data-id="' + post.id + '">'
                + ' delete' + '</a>').appendTo('.' + 'post' + post.id).on('click', deletePost);
                //$deletePostLink.on('click', deletePost);
                $('.js-wall-post').trigger('reset');
            });
        }
    });

    $('.del').on('click', deletePost);

    function deletePost(e) {
        e.preventDefault();
        var reqData = {
            wallPostId: $(this).data('id')
        };

        $.ajax('/user/profile/deleteWallPost', {
            type: 'POST',
            data: reqData
        }).done(function () {
            $('div.post' + reqData.wallPostId).remove();
        });
    }
});