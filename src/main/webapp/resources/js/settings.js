/**
 * Created by victor on 15/03/15.
 */

$(document).ready(function () {
    var $settings = $('.js-settings');
    $('.js-submit').on('click', function (e) {
        e.preventDefault();
        var form = new Forms();
        var reqData = {
            firstName: $('.js-firstName').val(),
            lastName: $('.js-lastName').val(),
            birthday: $('.js-birthday').val(),
            country: $('.js-country').val(),
            city: $('.js-city').val(),
            uri: $('.js-uri').val(),
            email: $('.js-email').val()
        };
        if ($('.js-male').is(':checked')) {
            reqData.gender = 'male'
        } else if ($('.js-female').is(':checked')) {
            reqData.gender = 'female';
        } else {
            reqData.gender = null;
        }
        var errEmailValid = form.validate('email', reqData.email);
        if (errEmailValid.err) {
            form.showErr($settings, errEmailValid.msg, true);
            return;
        }
        $.ajax('/user/settings', {
            type: 'POST',
            data: reqData
        }).done(function (response) {
            response = JSON.parse(response);
            if (response.error) {
                form.showErr($settings, response.errorMessage, true)
            } else if (response.redirectType) {
                document.location.href = response.redirectUrl;
            } else {
                $('.js-home-link').attr('href', '/user/profile/' + reqData.uri);
                alert('Settings successfully saved!');
            }
        });
    });
});