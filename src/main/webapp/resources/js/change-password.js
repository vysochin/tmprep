/**
 * Created by victor on 15/03/15.
 */
$(document).ready(function() {
    var $changePassword = $('.js-change-password');
    $('.js-submit').on('click', function (e) {
        e.preventDefault();
        var reqData = {
            oldPassword: $('.js-oldPassword').val(),
            newPassword: $('.js-newPassword').val(),
            repeatPassword: $('.js-repeatPassword').val()
        };

        var form = new Forms();
        var errPassValid = form.validate('pass', {
            pass: reqData.newPassword,
            confirmPass: reqData.repeatPassword
        });
        if (errPassValid.err) {
            form.showErr($changePassword, errPassValid.msg);
            return;
        }
        $.ajax({
            url: '/user/settings/password',
            type: 'POST',
            data: reqData
        }).done(function (response) {
            response = JSON.parse(response);
            if (response.error) {
                form.showErr($changePassword, response.errorMessage)
            } else if (response.redirectType) {
                document.location.href = response.redirectUrl;
            }
        });
    });
});