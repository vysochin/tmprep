/**
 * Created by victor on 28/03/15.
 */
function Chat () {
    this.path = window.location.pathname;
    this.stompClient = null;
    this.connect = function () {
        var self = this;
        var socket = new SockJS('/chat');
        this.stompClient = Stomp.over(socket);
        this.stompClient.connect({}, function(frame) {
            console.log('Connected: ' + frame);
            self.stompClient.subscribe('/topic/chat' + self.path,
                function(response){
                self.showGreeting(JSON.parse(response.body));
            });
        });
    };
    this.disconnect = function () {
        if (this.stompClient != null) {
            this.stompClient.disconnect();
        }
        console.log('Disconnected');
    };
    this.sendMessage = function (message) {
        this.stompClient.send("/app/chat" +  this.path, {}, JSON.stringify(message));
    };
    this.showGreeting = function (message, userId) {
        var $messageCont = $('.js-messages-container');
        $messageCont.append(
            '<li' + (message.authorId === $('.js-from').data('id') ? ' class="my-message" ' : '') + '>' +
                '<span class="author">' + message.authorName + '</span>' +
                '<span class="date">' + $.format.date(message.dateSent, "dd-MM-yyyy HH:mm:ss") + '</span>' +
                '<div class="message-text">' + message.messageBody + '</div>' +
            '</li>'
        );
        $messageCont.parent().scrollTop($messageCont.height())
    }
}

$(document).ready(function () {
    var $convTopic = $('.js-conv-topic'),
        currentTopic,
        setTopic = function (topic) {
            if (topic !== '' && topic !== currentTopic) {
                $.ajax('/conversation-settings/set-topic', {
                    type: 'post',
                    data: {
                        topic: topic,
                        convId: $convTopic.data('conv-id')
                    }
                });
            } else {
                $convTopic.find('.js-set-topic').val(currentTopic);
            }
        };

    $convTopic.on('submit', function (e) {
        e.preventDefault();
    });
    $convTopic.find('.js-set-topic').on('focus', function (e) {
        currentTopic = $(this).addClass('active').val();
    }).on('blur', function (e) {
        var _this = $(this);
        _this.removeClass('active');
        setTopic(_this.val());
    }).on('keyup', function (e) {
        if (e.keyCode === 13) {
            $(this).blur();
        }
    });
});
