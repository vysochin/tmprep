<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 23/02/15
  Time: 23:54
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
    <title>DenVik - Log in</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>" />
    <link rel="stylesheet" href="<c:url value="/webjars/bootstrap/3.3.1/css/bootstrap.css" />"/>
    <script src="<c:url value="/webjars/jquery/2.1.3/jquery.min.js"/>"></script>
    <script src="<c:url value="/webjars/bootstrap/3.3.1/js/bootstrap.min.js" />"></script>
</head>
<body>
<%@include file="/WEB-INF/partials/header.jsp" %>
<div class="wrapper">
    <jsp:doBody/>
</div>
</body>
</html>
