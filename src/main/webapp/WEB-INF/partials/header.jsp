<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 21.02.2015
  Time: 13:08
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#m-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="/" title="DenVik">
                <img src="/resources/images/logo.svg" alt="logo"/>
            </a>
        </div>
        <c:set var="userLogIn" value="${sessionScope.get('user')}" />
        <div class="collapse navbar-collapse" id="m-navbar-collapse">
            <c:choose>
                <c:when test="${pageScope.userLogIn ne null}">
                    <ul class="nav navbar-nav">
                        <%--@TODO make active menu items--%>
                        <li role="presentation">
                            <a href="/user/profile/${pageScope.userLogIn.getUri()}" class="js-home-link">Profile</a>
                        </li>
                        <li role="presentation"><a href="/user/messages">Messages</a></li>
                        <li role="presentation"><a href="/search?nameCriteria=&countryCriteria=&genderCriteria=">Search</a></li>
                        <li role="presentation"><a href="/user/friends">Friends</a></li>
                        <li role="presentation"><a href="/user/settings">Settings</a></li>
                        <li role="presentation"><a href="/logout">Log out</a></li>
                    </ul>
                </c:when>
                <c:otherwise><h1>${title}</h1></c:otherwise>
            </c:choose>
        </div>
    </div>
</nav>
