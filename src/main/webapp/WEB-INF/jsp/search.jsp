<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:default>
    <h1>${title}</h1>

    <form action="/search" method="get" class="form-inline">
        <div class="form-group">
            <label class="search">Name</label>
            <input type="search" id="search" name="nameCriteria" placeholder="Name Surname" class="form-control"/>
        </div>
        <div class="form-group">
            <label for="countryCriteria">Country: </label>
            <select name="countryCriteria" id="countryCriteria" class="form-control">
                <option>All</option>
                <c:forEach items="${countries}" var="country">
                    <option value="${country.country}">${country.displayCountry}</option>
                </c:forEach>
            </select>
        </div>
        <div class="form-group">
            <label for="genderCriteria">Gender:</label>
            <select name="genderCriteria" id="genderCriteria" class="form-control">
                <option>All</option>
                <option>Male</option>
                <option>Female</option>
            </select>
        </div>
        <button type="submit" class="btn btn-default">Search</button>
    </form>
    <c:choose>
        <c:when test="${!empty users}">
            <ul class="users-list">
                <c:forEach items="${users}" var="user">
                    <li>
                        <a href="/user/profile/${user.uri}">
                            <c:choose>
                                <c:when test="${not empty user.avatarName}">
                                    <img src="/resources/images/avatars/${user.avatarName}" width="40" height="40">
                                </c:when>
                                <c:otherwise>
                                    <img src="/resources/images/avatars/noAvatar.png" width="40">
                                </c:otherwise>
                            </c:choose>
                            <h3>${user.firstName} ${user.lastName}</h3>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
            <h3>Not Found</h3>
        </c:otherwise>
    </c:choose>
</t:default>