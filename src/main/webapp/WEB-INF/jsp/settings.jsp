<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:default>
    <h1>${title}</h1>

    <c:choose>
        <c:when test="${not empty user.avatarName}">
            <img src="/resources/images/avatars/${user.avatarName}" width="190">
        </c:when>
        <c:otherwise>
            <img src="/resources/images/avatars/noAvatar.png" width="190">
        </c:otherwise>
    </c:choose>
    <form action="/user/settings/set/photo/" method="POST" enctype="multipart/form-data">
        <p><input name="image" type="file"></p>
        <p><input type="submit" class="btn btn-primary" name="Upload Photo" value="Upload Avatar"></p>
    </form>

    <form action="" method="post" class="form-horizontal js-settings">
        <fieldset>
            <div class="form-group">
                <label for="firstName">Name</label>
                <input type="text" class="form-control js-firstName" id="firstName" name="firstName"
                       value="${sessionScope.get("user").firstName}" placeholder="Type your first name" />
            </div>
            <div class="form-group">
                <label for="lastName">Surname</label>
                <input type="text" class="form-control js-lastName" id="lastName" name="lastName"
                       value="${sessionScope.get("user").lastName}" placeholder="Type your last name" />
            </div>
            <div class="form-group">
                <label class="radio-inline">
                    <input type="radio" name="gender" id="male" value="male" class="js-male"
                        ${sessionScope.get('user').gender eq 'male' ? 'checked' : ''}>
                    male
                </label>
                <label class="radio-inline">
                    <input type="radio" name="gender" id="female" value="female" class="js-female"
                        ${sessionScope.get('user').gender eq 'female' ? 'checked' : ''}>
                    female
                </label>
            </div>
            <div class="form-group">
                <label for="birthday">Birthday</label>
                <input type="date" name="birthday" id="birthday" class="form-control js-birthday">
            </div>
            <div class="form-group">
                <label for="country">Where are you from?</label>
                <select name="country" id="country" class="form-control js-country">
                    <c:forEach var="country" items="${countries}">
                        <%--<option value="${country.getCountry()}">country.getDisplayCountry()</option>--%>
                        <c:choose>
                            <c:when test="${country eq sessionScope.get('user').country}">
                                <option value="${country}" selected="selected">${country}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${country}">${country}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="city">City</label>
                <input type="text" id="city" class="form-control js-city" name="city"
                       value="${sessionScope.get("user").city}" placeholder="Type your city" />
            </div>
            <div class="form-group">
                <label for="uri">Change uri</label>
                <input type="text" id="uri" class="form-control js-uri" name="uri"
                       value="${sessionScope.get("user").uri}" placeholder="Type your uri" />
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" class="form-control js-email" name="email"
                       value="${sessionScope.get("user").email}" placeholder="Type your email" />
            </div>
            <div class="form-group">
                <a href="/user/settings/password">Change your password</a>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary js-submit">Save</button>
            </div>
        </fieldset>
    </form>
    <script src="/resources/js/libs/jquery-dateFormat.min.js"></script>
    <script src="/resources/js/forms.js"></script>
    <script src="/resources/js/settings.js"></script>
    <script>
        $(document).ready(function () {
            $('.js-birthday').val($.format.date('${sessionScope.get('user').birthday}', 'yyyy-MM-dd'));
        });
    </script>
</t:default>
