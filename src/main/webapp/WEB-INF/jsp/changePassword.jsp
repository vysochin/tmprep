<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:default>
    <h1>Changing password</h1>

    <form action="/user/settings/password" method="post" class="js-change-password">
        <fieldset>
            <div class="form-group">
                <label for="oldPassword">Old password</label>
                <input type="password" name="oldPassword" class="form-control js-oldPassword"
                       id="oldPassword" placeholder="Type your old password here" />
            </div>
            <div class="form-group">
                <label for="newPassword">New password</label>
                <input type="password" name="newPassword" class="form-control js-newPassword"
                       id="newPassword" placeholder="Type your new password here"/>
            </div>
            <div class="form-group">
                <label for="repeatPassword">Repeat password</label>
                <input type="password" name="repeatPassword" class="form-control js-repeatPassword"
                       id="repeatPassword" placeholder="Confirm your new password"/>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default js-submit">Save changes</button>
            </div>
        </fieldset>
    </form>
    <script src="/resources/js/forms.js"></script>
    <script src="/resources/js/change-password.js"></script>
</t:default>