<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:default>
    <form class="login-form well js-sign-in">
        <div class="form-group js-field-reg field-reg">
            <label for="name">Your name</label>
            <input type="text" class="form-control js-name" id="name" placeholder="Enter your name">
        </div>
        <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control js-email" id="email" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control js-pass" id="password" placeholder="Password">
        </div>
        <div class="form-group field-reg js-field-reg">
            <label for="password">Confirm password</label>
            <input type="password" class="form-control js-confirm-pass" id="confirmPassword" placeholder="Repeat your password">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" class="js-register"/> I am a new user
            </label>
        </div>
        <button type="submit" class="btn btn-primary js-submit">Submit</button>
    </form>
    <script src="/resources/js/forms.js"></script>
    <script src="/resources/js/sign-in.js"></script>
</t:default>