<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<t:default>
    <h1>${title}</h1>

    <div class="panel">
        <h2>${wrapper.currentUser.firstName} ${wrapper.currentUser.lastName}</h2>
        <c:choose>
            <c:when test="${not empty wrapper.currentUser.avatarName}">
                <%--<img src="<spring:url value='/res/${wrapper.currentUser.avatarName}'/>" width="300" height="300">--%>
                <img src="/resources/images/avatars/${wrapper.currentUser.avatarName}" width="300">
            </c:when>
            <c:otherwise>
                <img src="/resources/images/avatars/noAvatar.png" width="192">
            </c:otherwise>
        </c:choose>
        <div class="profileMenu">
            <c:choose>
                <c:when test="${wrapper.isMe eq true}">
                    <p>Its me!</p>

                </c:when>
                <c:otherwise>
                    <c:choose>
                        <c:when test="${wrapper.isUserBlackListed eq true}">
                            <p class="info">You are blacklisted</p>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${wrapper.isMyFriend eq true}">
                                    <p class="info">${wrapper.currentUser.firstName} your friend</p>
                                    <a href="/user/message/${wrapper.currentUser.uri}" class="message">Send message</a>
                                    <a href="/" class="deleteFriend boldRef"
                                       data-id="${wrapper.currentUser.id}">Delete
                                        friend</a>

                                </c:when>
                                <c:otherwise>
                                    <c:choose>
                                        <c:when test="${wrapper.isNotConfirmedMyRequest eq true}">
                                            <p class="info">You sent the invitation</p>
                                        </c:when>
                                        <c:when test="${wrapper.isNotConfirmedFriend eq true}">
                                            <a href="/" class="confirm boldRef"
                                               data-id="${wrapper.currentUser.id}">Confirm
                                                invitation</a>
                                            <a href="/" class="deleteFriend boldRef"
                                               data-id="${wrapper.currentUser.id}">Ignore</a>

                                        </c:when>
                                        <c:otherwise>
                                            <a href="/" class="addFriend boldRef"
                                               data-id="${wrapper.currentUser.id}">Add
                                                friend</a>

                                        </c:otherwise>
                                    </c:choose>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${wrapper.isCurrentUserBlackListed eq true}">
                            <a href="/" class="blackListRemove boldRef"
                               data-id="${wrapper.currentUser.id}">Remove from
                                Black
                                List</a>

                        </c:when>
                        <c:otherwise>
                            <a href="/" class="blackListAdd boldRef"
                               data-id="${wrapper.currentUser.id}">Add to
                                Black
                                List</a>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </div>
        <c:if test="${(not empty wrapper.currentUser.country) ||  (not empty wrapper.currentUser.birthday)}">
            <h3>Describe</h3>
            <c:if test="${not empty wrapper.currentUser.country}">
                <p>Country : ${wrapper.currentUser.country} </p>
            </c:if>
            <c:if test="${not empty wrapper.currentUser.birthday}">
                <p>Birthday : <span class="date"><fmt:formatDate type="date" value="${wrapper.currentUser.birthday}"/></span></p>
            </c:if>
        </c:if>
    </div>
    <div class="mainContent">
        <c:if test="${!wrapper.isUserBlackListed}">
            <h3>Wall</h3>

            <%--<form action="/user/profile/addWallPost/?toUserId=${wrapper.currentUser.id}" method="POST" class="form">--%>
        <form action="" method="post" class="js-wall-post">
            <div class="form-group">
                    <textarea name="postText" id="message" rows="5" cols="18" placeholder="Type your post here"
                              class="form-control js-text" data-id="${wrapper.currentUser.id}"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default js-submit">Post</button>
            </div>
        </form>
        <div class="wall">
            <c:forEach items="${wallList}" var="post">
                <div class="post${post.wallPost.id}">
                    <hr>
                    <c:choose>
                        <c:when test="${not empty post.author.avatarName}">
                            <img src="/resources/images/avatars/${post.author.avatarName}" width="30" height="30">
                        </c:when>
                        <c:otherwise>
                            <img src="/resources/images/avatars/noAvatar.png" width="30">
                        </c:otherwise>
                    </c:choose>

                    <a class="boldRef"
                       href="/user/profile/${post.author.uri}">
                            ${post.author.firstName}
                            ${post.author.lastName}</a>
                    <span class="date"><fmt:formatDate type="both" dateStyle="medium" timeStyle="medium"
                                                       value="${post.wallPost.date}"/></span>
                    <br>

                    <p class="wall-text"> ${post.wallPost.text}</p>
                    <c:if test="${wrapper.isMe or post.author.id eq sessionScope.get('user').id}">
                        <a href="/" class="del" data-id="${post.wallPost.id}">delete</a>

                    </c:if>
                </div>
            </c:forEach>
            </c:if>
        </div>
    </div>
    <script src="/resources/js/libs/jquery-dateFormat.min.js"></script>
    <script src="/resources/js/wall.js"></script>
    <script src="/resources/js/friend.js"></script>
</t:default>