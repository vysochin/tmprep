<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:default>
    <h1>${title}</h1>
    <img src="/resources/images/error-icon.png" alt="image"/>
    <p>${errorMessage}</p>
</t:default>