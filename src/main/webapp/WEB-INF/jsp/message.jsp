<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:default>
    <h1>${title}</h1>
    <form action="/message" method="post" class="form">
        <div class="form-group">
            <label for="message">Message</label>
            <textarea name="message" id="message" rows="10" cols="45" placeholder="Type your message here" class="form-control"></textarea>
        </div>
        <input type="submit" class="btn btn-default">
    </form>
</t:default>