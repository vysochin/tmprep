<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<t:default>
    <h1>${title}</h1>
    <ul class="messages-list">
        <c:if test="${fn:length(conversations) eq 0}">
            <p>You haven't got messages yet</p>
        </c:if>
        <c:forEach items="${conversations}" var="conv">
            <li>
                <a href="/conversation/${conv.uri}">
                    <h3>${conv.topic}</h3>
                    <span class="member">Member:
                        <c:choose>
                            <c:when test="${conv.creatorId eq sessionScope.get('user').id}">
                                ${conv.memberName}
                            </c:when>
                            <c:otherwise>
                                ${conv.creatorName}
                            </c:otherwise>
                        </c:choose>
                    </span>
                </a>
            </li>
        </c:forEach>
    </ul>
</t:default>
