<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:default>
    <h1>${title}</h1>

    <c:if test="${not empty notConfirmedFriends}">
        <h2>Not confirmed friends : </h2>
        <ul class="users-list">
            <c:forEach items="${notConfirmedFriends}" var="user">
                <li>
                    <a href="/user/profile/${user.uri}">
                        <c:choose>
                            <c:when test="${not empty user.avatarName}">
                                <img src="/resources/images/avatars/${user.avatarName}" width="40" height="40">
                            </c:when>
                            <c:otherwise>
                                <img src="/resources/images/avatars/noAvatar.png" width="40">
                            </c:otherwise>
                        </c:choose>
                        <h3>${user.firstName} ${user.lastName}</h3>
                    </a>
                </li>
            </c:forEach>
        </ul>
    </c:if>
    <h2>Your friends : </h2>
    <c:choose>
        <c:when test="${empty confirmedFriends}">
            <p>You don`t have a friends</p>
        </c:when>
        <c:otherwise>
            <ul class="users-list">
                <c:forEach items="${confirmedFriends}" var="user">
                    <li>
                        <a href="/user/profile/${user.uri}">
                            <c:choose>
                                <c:when test="${not empty user.avatarName}">
                                    <img src="/resources/images/avatars/${user.avatarName}" width="40" height="40">
                                </c:when>
                                <c:otherwise>
                                    <img src="/resources/images/avatars/noAvatar.png" width="40">
                                </c:otherwise>
                            </c:choose>
                            <h3>${user.firstName} ${user.lastName}</h3>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </c:otherwise>
    </c:choose>
    <ul class="users-list">
        <c:forEach items="${friends}" var="user">
            <li>
                <a href="/user/profile/${user.uri}">
                    <h3>${user.firstName} ${user.lastName}</h3>
                    <img src="/resources/images/avatars/${user.uri}.jpg" width="40" height="40" />
                </a>
            </li>
        </c:forEach>
    </ul>
</t:default>
