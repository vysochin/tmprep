<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:default>
    <form action="/conversation-settings/set-topic/${conversation.uri}"
          class="conv-topic js-conv-topic" data-conv-id="${conversation.id}">
        <h1>
            <label>
                <input type="text" value="${conversation.topic}" class="js-set-topic" />
            </label>
        </h1>
    </form>
    <div class="messages-container">
        <ul class="js-messages-container">
            <c:forEach items="${messages}" var="message">
                <c:choose>
                    <c:when test="${message.authorId eq sessionScope.get('user').id}">
                        <li class="my-message">
                    </c:when>
                    <c:otherwise>
                        <li>
                    </c:otherwise>
                </c:choose>
                    <span class="author">${message.authorName}</span>
                    <span class="date">${message.dateSent}</span>
                    <div class="message-text">${message.messageBody}</div>
                </li>
            </c:forEach>
        </ul>
    </div>
    <form action="/message" method="post" class="form js-send-message" data-id="${sessionScope.get('user').id}">
        <div class="form-group">
            <label for="message">Message</label>
            <textarea name="message" id="message" rows="5" cols="45"
                      placeholder="Type your message here" class="form-control js-message"></textarea>
        </div>
        <input type="submit" class="js-submit btn btn-default">
    </form>
    <script src="/resources/js/libs/jquery-dateFormat.min.js"></script>
    <script src="/resources/js/libs/sockjs-0.3.4.js"></script>
    <script src="/resources/js/libs/stomp.js"></script>
    <script src="/resources/js/forms.js"></script>
    <script src="/resources/js/Chat.js"></script>
    <script>
        $(document).ready(function () {
            var chat = new Chat();
            chat.connect();
            var chatForm = new Forms();
            /**
             * Event handlers UI
             */
            var $messageTxt = $('.js-message');
            $('.js-submit').on('click', sendMessage);
            $messageTxt.on('keydown', function (e) {
                if (e.keyCode === 13) {
                    sendMessage(e);
                }
            });

            function sendMessage(e) {
                e.preventDefault();
                if ($messageTxt.val() !== '') {
                    chat.sendMessage({
                        conversationId: ${conversation.id},
                        authorId: ${sessionScope.get('user').id},
                        recipientId: ${recipientId},
                        authorName: '${sessionScope.get('user').firstName} ${sessionScope.get('user').lastName}',
                        dateSent: new Date(),
                        messageBody: $messageTxt.val()
                    });
                    $messageTxt.val('');
                } else {
                    chatForm.showErr($('.js-send-message'), 'Message couldn\'t be empty', true)
                }
            }
        });
    </script>
</t:default>