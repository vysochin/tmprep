package app;

import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Repository
@Transactional
public class FormValidator {

    private Pattern pattern;
    private Matcher matcher;

    public String getErroressage() {
        return erroressage;
    }

    public void setErroressage(String erroressage) {
        this.erroressage = erroressage;
    }

    private String erroressage;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public FormValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    /**
     * Validate hex with regular expression
     *
     * @param hex
     *            hex for validation
     * @return true valid hex, false invalid hex
     */
    public boolean validateEmail(final String hex) {
        if (! pattern.matcher(hex).matches()) {
            setErroressage("Email is not valid");
            return false;
        }
        return true;
    }

    public boolean validatePassConfirm (String pass, String confirmPass) {
        if (! pass.equals(confirmPass)) {
            setErroressage("Password doesn't match");
            return false;
        }
        if (pass.length() < 6) {
            setErroressage("Password length should be more than 6 characters");
            return false;
        }
        return true;
    }
}