package app.dao;

import app.entity.Conversation;
import app.entity.User;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by victor on 09/04/15.
 */
@Repository
@Transactional
public class ConversationDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Conversation conversation) {
        sessionFactory.getCurrentSession().saveOrUpdate(conversation);
    }

    public void delete(Conversation conversation) {
        sessionFactory.getCurrentSession().delete(conversation);
    }

    public Conversation getConversationById(int id) {
        return (Conversation) sessionFactory.getCurrentSession().createCriteria(Conversation.class)
                .add(Restrictions.eq("id", id)).uniqueResult();
    }

    public Conversation getConversationByUri(String uri) {
        return (Conversation) sessionFactory.getCurrentSession().createCriteria(Conversation.class)
                .add(Restrictions.eq("uri", uri)).uniqueResult();
    }

    public Conversation getConversationByMembers(User member1, User member2) {
        String hql = "select conversation from Conversation conversation " +
                "where (conversation.creatorId = :member1Id " +
                "and conversation.memberId = :member2Id) " +
                "or (conversation.creatorId = :member2Id " +
                "and conversation.memberId = :member1Id)";
        return (Conversation) sessionFactory.getCurrentSession()
                .createQuery(hql).setInteger("member1Id", member1.getId())
                .setInteger("member2Id", member2.getId())
                .uniqueResult();
    }

    public Conversation checkConversationMember(String convUri, Integer userId) {
        return (Conversation) sessionFactory.getCurrentSession()
                .createQuery("select conversation from Conversation conversation " +
                        "where conversation.uri = :convUri and " +
                        "(conversation.creatorId = :userId or " +
                        "conversation.memberId = :userId)")
                .setString("convUri", convUri).setInteger("userId", userId)
                .uniqueResult();
    }


    public List<Conversation> getConversations(int id) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Conversation conversation " +
                        "where conversation.creatorId = :userId or " +
                        "conversation.memberId = :userId").setInteger("userId", id).list();
    }
}
