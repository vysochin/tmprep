package app.dao;

import app.util.PasswordSalts;
import app.entity.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.security.GeneralSecurityException;
import java.util.List;


/**
 * Created by admin on 07.03.2015.
 */

@Repository
@Transactional
public class UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public List getUsers() {

        return sessionFactory.getCurrentSession().createCriteria(User.class)
                .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
    }

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    public void updateUser(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    public void mergeUser(User user) {
        sessionFactory.getCurrentSession().merge(user);
    }

    public User createUser(String email, byte[] password, byte[] salt, String firstName) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        user.setSalt(salt);
        user.setFirstName(firstName);
        saveUser(user);
        return user;
    }

    public User getUserByEmailAndPassword(String email, String password) {
        String hql = "select user from User user where user.email = :userEmail";

        User user = (User) sessionFactory.getCurrentSession()
                    .createQuery(hql).setString("userEmail", email).uniqueResult();
        try {
            return PasswordSalts.matches(password.toCharArray(),
                    user.getPassword(),
                    user.getSalt()) ?
                    user : null;
        } catch (GeneralSecurityException e) {
            return null;
        }
    }

    public User getUserByEmail(String email) {
        return (User)sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult();
    }


    public User getUserByUri(String uri) {
        return (User)sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("uri", uri)).uniqueResult();
    }

    public User getUserById(int id) {
        return (User)sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("id", id)).uniqueResult();
    }
    public List getUsersByCriteria(String nameCriteria, String countryCriteria, String genderCriteria) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);

        if (!countryCriteria.equals("All")){
            criteria.add(
                    Restrictions.like("country", countryCriteria)
            );
        }

        if (!genderCriteria.equals("All")){
            criteria.add(
                    Restrictions.eq("gender", genderCriteria)
            );
        }

        String[] criterias = nameCriteria.split(" ");
        if (!nameCriteria.isEmpty()) {
            for (String crit : criterias) {
                criteria.add(Restrictions.or(
                        Restrictions.like("firstName", crit),
                        Restrictions.like("lastName", crit)
                ));
            }
        }
        return criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
    }
}