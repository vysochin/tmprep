package app.dao;

import app.entity.User;
import app.entity.WallPost;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 05.04.2015.
 */
@Repository
@Transactional
public class WallPostDao {

    @Autowired
    private SessionFactory sessionFactory;

    public WallPost getWallPostById(int id){
        return (WallPost)sessionFactory.getCurrentSession().createCriteria(WallPost.class).add(Restrictions.eq("id", id)).uniqueResult();
    }

    public List getWallPostListByUser(User user){
        return sessionFactory.getCurrentSession().createCriteria(WallPost.class).add(Restrictions.eq("toUserId", user.getId())).list();
    }

    public void delete(WallPost wallPost){
        if(sessionFactory.getCurrentSession().contains(wallPost)) {
            sessionFactory.getCurrentSession().delete(wallPost);
        }
    }

    public void save(WallPost wallPost){
        sessionFactory.getCurrentSession().saveOrUpdate(wallPost);
    }

    public int addWallPost(HttpSession session, int toUserId, String postText){
        User user = (User)session.getAttribute("user");
        WallPost wallPost = new WallPost();
        wallPost.setFromUserId(user.getId());
        wallPost.setToUserId(toUserId);
        wallPost.setText(postText);
        wallPost.setDate(new Date());
        save(wallPost);
        return wallPost.getId();
    }

    public void deleteWallPost(WallPost wallPost){
        delete(wallPost);
    }
}
