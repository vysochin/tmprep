package app;

import app.entity.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by admin on 12.03.2015.
 */
@Repository
@Transactional
public class FiltrationManager {

    @Autowired
    SessionFactory sessionFactory;

    public List getListByCriteria(String nameCriteria, String countryCriteria, String ganderCriteria,Class searchClass) {


        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);

        if (!countryCriteria.equals("All")){
            criteria.add(
                    Restrictions.eq("country", countryCriteria)
            );
        }

        if (!ganderCriteria.equals("All")){
            criteria.add(
                    Restrictions.eq("gander", ganderCriteria)
            );
        }

        String[] words = nameCriteria.split(" ");
        if (!nameCriteria.isEmpty()) {
            for (String word : words) {
                criteria.add(Restrictions.or(
                        Restrictions.eq("firstName", word),
                        Restrictions.eq("lastName", word)
                ));
            }
        }
        return criteria.list();
    }
}
