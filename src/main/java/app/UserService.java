package app;

import app.entity.User;
import net.sf.ehcache.hibernate.HibernateUtil;
import org.apache.catalina.Session;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;


/**
 * Created by admin on 07.03.2015.
 */

@Repository
@Transactional
public class UserService {
    @Autowired
    private SessionFactory sessionFactory;

    public List getUsers() {

        return sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .list();
    }

    public List getUsersByCriteria(String nameCriteria, String countryCriteria, String genderCriteria) {
        /*FiltrationManager filtrationManager = new FiltrationManager();
        return filtrationManager.getListByCriteria(nameCriteria, countryCriteria, genderCriteria, User.class);*/

        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);

        if (!countryCriteria.equals("All")){
            criteria.add(
                    Restrictions.eq("country", countryCriteria)
            );
        }

        if (!genderCriteria.equals("All")){
            criteria.add(
                    Restrictions.eq("gender", genderCriteria)
            );
        }

        String[] words = nameCriteria.split(" ");
        if (!nameCriteria.isEmpty()) {
            for (String word : words) {
                criteria.add(Restrictions.or(
                        Restrictions.eq("firstName", word),
                        Restrictions.eq("lastName", word)
                ));
            }
        }
        return criteria.list();
    }

    public void saveUser(User user) {
        //try {
            /*org.hibernate.Session session = sessionFactory.openSession();
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            session.close();
        //} catch (HibernateException e){
            //System.out.print(e);
        //}*/
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    public void mergeUser(User user) {
        sessionFactory.getCurrentSession().merge(user);
    }

    public User createUser(String email, String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        saveUser(user);
        return user;
    }

    public User getUserByEmailAndPassword(String email, String password) {
        String hql = "select user from User user where user.email = :userEmail " +
                "and user.password = :userPassword";
            return (User) sessionFactory.getCurrentSession()
                    .createQuery(hql).setString("userEmail", email)
                    .setString("userPassword", password).uniqueResult();
    }

    public User getUserByEmail(String email) {
        String hql = "select user from User user where user.email = :userEmail";
        return (User) sessionFactory.getCurrentSession()
                .createQuery(hql).setString("userEmail", email).uniqueResult();
    }


    public User getUserByUri(String uri) {
        String hql = "select user from User user where user.uri = :uri";

        return (User) sessionFactory.getCurrentSession()
                .createQuery(hql).setString("uri", uri).uniqueResult();
    }
}