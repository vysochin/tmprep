/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.UserService;
import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class Profile extends HttpServlet {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user/profile/*", method = RequestMethod.GET)
    public String profile(HttpServletRequest request, HttpSession session, Map<String, Object> model) {

        String userUri = request.getServletPath().replace("/user/profile/", "");
        User user = (User) session.getAttribute("user");
        User currentUser = userService.getUserByUri(userUri);
        Integer flag=0;

        if (!user.equals(currentUser)){
            if (user.getFriends().contains(currentUser)) {
                flag = 1;
            } else {
                flag = 2;
            }
        }

        model.put("flag", flag);
        model.put("title", "Profile");
        model.put("user", userService.getUserByUri(userUri));
        return RegisterLogIn.loggedIn(session) ? "profile" : "redirect:/";
    }
}
