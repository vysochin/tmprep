/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.entity.Conversation;
import app.entity.User;
import app.services.ConversationService;
import app.services.MessageService;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class MessageController extends HttpServlet {
    @Autowired
    MessageService messageService;

    @Autowired
    UserService userService;
    @Autowired
    ConversationService conversationService;

    @RequestMapping(value = "/user/message/{userUri}", method = RequestMethod.GET)
    public String message(
            HttpSession session,
            @PathVariable("userUri") String userUri,
            Map<String, Object> model
    ) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        User memberAuth = (User) session.getAttribute("user");
        User member = userService.getUserByUri(userUri);
        Conversation conversation =
                conversationService.createConversation(memberAuth, member);
        return "redirect:/conversation/" + conversation.getUri();
    }

    @RequestMapping(value = "/user/messages", method = RequestMethod.GET)
    public String getConversations(
            Map<String, Object> model,
            HttpSession session
    ) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        model.put("title", "Messages");
        User currentUser = (User) session.getAttribute("user");
        model.put("conversations", conversationService.getConversations(currentUser.getId()));
        return "messages";
    }
}
