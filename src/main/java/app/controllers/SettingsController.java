/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.ajax.AjaxResponse;
import app.entity.User;
import app.services.ImageService;
import app.services.SettingsService;
import app.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
public class SettingsController extends HttpServlet {
    @Autowired
    UserService userService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private SettingsService settingsService;

    private ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    @RequestMapping(value = "/user/settings", method = RequestMethod.GET)
    public String settings(HttpSession session, Map<String, Object> model) {
        User user = (User) session.getAttribute("user");
        if (!RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        model.put("title", "Settings");
        List<String> countries = new ArrayList<>();
        for (String locale : Locale.getISOCountries()) {
            countries.add(new Locale("", locale).getDisplayCountry());
        }
        model.put("countries", countries);
        model.put("imagePath", new File("C:\\Users\\admin\\Desktop\\dir\\geekhub-social-network\\src\\main\\webapp\\resources\\images\\avatars\\" + user.getUri() + ".jpg").exists()
                ? "/resources/images/avatars/" + user.getUri() + ".jpg" : null);
        return "settings";
    }

    @RequestMapping(value = "/user/settings", method = RequestMethod.POST)
    @ResponseBody
    public String setSettings(
            HttpSession session,
            HttpServletRequest request) {
        AjaxResponse ajaxResponse;
        if (!RegisterLogInController.loggedIn(session)) {
            ajaxResponse = new AjaxResponse(false);
            ajaxResponse.setRedirectType(true);
            ajaxResponse.setRedirectUrl("/");
        } else {
            ajaxResponse = settingsService.setSettings(request,
                    (User) session.getAttribute("user"));
        }
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/settings/password", method = RequestMethod.GET)
    public String changePassword(
            HttpSession session,
            Map<String, ObjectMapper> model
    ) {
        if (!RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        return "changePassword";
    }

    @RequestMapping(value = "/user/settings/password", method = RequestMethod.POST)
    @ResponseBody
    public String setPassword(
            HttpServletRequest request,
            HttpSession session
    ) {
        AjaxResponse ajaxResponse;
        if (!RegisterLogInController.loggedIn(session)) {
            ajaxResponse = new AjaxResponse(false);
            ajaxResponse.setRedirectType(true);
            ajaxResponse.setRedirectUrl("/");
        } else {
            ajaxResponse = settingsService.changePassword(
                    request, (User) session.getAttribute("user"));
        }
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/settings/set/photo/*", method = RequestMethod.POST)
    public String addPersonFromForm(@RequestParam(value = "image") MultipartFile image,
                                    HttpSession session) {
        AjaxResponse ajaxResponse = new AjaxResponse(false);
        if (!RegisterLogInController.loggedIn(session)) {
            ajaxResponse.setRedirectType(true);
            ajaxResponse.setRedirectUrl("/");
        }
        if (image.isEmpty()) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("File is empty");
            System.out.println("File is empty");
        } else {
            String imageExtension = imageService.getFileExtension(image.getOriginalFilename());
            if (!(imageExtension.equals(".jpg")
                    || imageExtension.equals(".png")
                    || imageExtension.equals(".gif"))) {
                ajaxResponse.setError(true);
                ajaxResponse.setErrorMessage("Allowable extension : jpg, png, gif");
                System.out.println("Allowable extension : jpg, png, gif");
            } else {
                imageService.saveImage(image, session);
                ajaxResponse.setRedirectType(true);
                ajaxResponse.setRedirectUrl("/user/settings");
            }
        }
        return "redirect:/user/settings";
    }
}
