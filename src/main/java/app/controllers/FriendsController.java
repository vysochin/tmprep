/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.ajax.AjaxProfile;
import app.ajax.AjaxResponse;
import app.util.Updater;
import app.dao.UserDao;
import app.entity.User;
import app.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class FriendsController extends HttpServlet {

    @Autowired
    UserDao userDao;

    @Autowired
    UserService userService;

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    Updater updater;


    @RequestMapping(value = "/user/friends", method = RequestMethod.GET)
    public String friends(HttpSession session, Map<String, Object> model) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        updater.updateDataOnClient(session);
        User user = (User) session.getAttribute("user");

        List friendList = user.getFriends();
        List<Object> confirmedFriends = new ArrayList<>();
        List<Object> notConfirmedFriends = new ArrayList<>();
        for (Object friend: friendList) {
            if (((User) friend).containsFriend(user)){
                confirmedFriends.add(friend);
            } else {
                notConfirmedFriends.add(friend);
            }
        }
        model.put("title", "Friends");
        model.put("confirmedFriends", confirmedFriends);
        model.put("notConfirmedFriends", notConfirmedFriends);

        return "friends";
    }

    @RequestMapping(value = "/user/friend/add", method = RequestMethod.POST)
    @ResponseBody
    public String addFriend(HttpServletRequest request, HttpSession session, Map<String, Object> model) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        AjaxResponse ajaxResponse = new AjaxResponse(false);

        updater.updateDataOnClient(session);
        String newFriendUri = userDao.getUserById(Integer.parseInt(request.getParameter("userId"))).getUri();
        userService.addFriend(session, newFriendUri);
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/friend/confirm", method = RequestMethod.POST)
    @ResponseBody
    public String confirmFriend(HttpServletRequest request, HttpSession session, Map<String, Object> model) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        AjaxProfile ajaxProfile = new AjaxProfile();
        updater.updateDataOnClient(session);
        String friendUri = userDao.getUserById(Integer.parseInt(request.getParameter("userId"))).getUri();
        ajaxProfile.setName(userDao.getUserByUri(friendUri).getFirstName());
        ajaxProfile.setUri(userDao.getUserByUri(friendUri).getUri());
        userService.confirmFriend(session, friendUri);
        try {
            return ow.writeValueAsString(ajaxProfile);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/friend/delete", method = RequestMethod.POST)
    @ResponseBody
    public String deleteFriend(HttpServletRequest request, HttpSession session, Map<String, Object> model) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        AjaxResponse ajaxResponse = new AjaxResponse(false);

        updater.updateDataOnClient(session);
        String friendUri = userDao.getUserById(Integer.parseInt(request.getParameter("userId"))).getUri();
        userService.deleteFriend(session, friendUri);
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/blackList/add", method = RequestMethod.POST)
    @ResponseBody
    public String addUserInBlackList(HttpServletRequest request, HttpSession session, Map<String, Object> model) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        AjaxResponse ajaxResponse = new AjaxResponse(false);

        updater.updateDataOnClient(session);
        String friendUri = userDao.getUserById(Integer.parseInt(request.getParameter("userId"))).getUri();
        userService.addUserInBlackList(session, friendUri);
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/blackList/remove", method = RequestMethod.POST)
    @ResponseBody
    public String removeUserFromBlackList(HttpServletRequest request, HttpSession session, Map<String, Object> model) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        AjaxResponse ajaxResponse = new AjaxResponse(false);

        updater.updateDataOnClient(session);
        String friendUri = userDao.getUserById(Integer.parseInt(request.getParameter("userId"))).getUri();
        userService.removeUserFromBlackList(session, friendUri);
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }
}
