/*
 * Copyright 2012-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package app.controllers;

import app.ajax.AjaxResponse;
import app.services.RegisterLoginService;
import app.services.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class RegisterLogInController {
    @Autowired
    UserService userService;
    @Autowired
    RegisterLoginService registerLoginService;

    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    @RequestMapping(value = {"/"}, method = {RequestMethod.GET, RequestMethod.HEAD})
	public String welcome(HttpSession session, Map<String, Object> model) {
        if (loggedIn(session)) {
            model.put("title", "Home");
            model.put("user", session.getAttribute("user"));
            return "home";
        }
        model.put("title", "Social network \"DenVik\"");
		return  "welcome";
	}

	@RequestMapping(value = {"/logout"}, method = {RequestMethod.GET})
    public String logout(HttpSession session, Map<String, Object> model) {
        if (loggedIn(session)) {
            session.invalidate();
        }
        model.put("title", "Social network \"DenVik\"");
        return "welcome";
    }

    @RequestMapping(value = {"/user/register"}, method = {RequestMethod.POST})
    @ResponseBody
    public String register(
            HttpSession session,
            Map<String, Object> model,
            HttpServletRequest request
    ) {
        AjaxResponse ajaxResponse = registerLoginService.regUser(request, session);
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (JsonProcessingException e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = {"/user/login"}, method = {RequestMethod.POST})
    @ResponseBody
    public String login(HttpSession session, Map<String, Object> model, HttpServletRequest request) {
        AjaxResponse ajaxResponse = registerLoginService.loginUser(request, session);
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (JsonProcessingException e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    public static boolean loggedIn(HttpSession session) {
        return session.getAttribute("user") != null;
    }
}
