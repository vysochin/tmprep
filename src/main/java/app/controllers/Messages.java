/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class Messages extends HttpServlet {
    @RequestMapping(value = "/user/message", method = RequestMethod.GET)
    public String message(HttpSession session, Map<String, Object> model) {
        if (! RegisterLogIn.loggedIn(session)) {
            return "redirect:/";
        }
        model.put("title", "Message");
        return "message";
    }
}
