/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.AjaxResponse;
import app.CountryService;
import app.FormValidator;
import app.UserService;
import app.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
public class Settings extends HttpServlet {
    @Autowired
    CountryService countryService;
    @Autowired
    UserService userService;
    @Autowired
    FormValidator formValidator;

    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    @RequestMapping(value = "/user/settings", method = RequestMethod.GET)
    public String settings(HttpSession session, Map<String, Object> model) {
        if (! RegisterLogIn.loggedIn(session)) {
            return "redirect:/";
        }
        model.put("title", "Settings");
        List<Locale> locales = new ArrayList<>();
        for (String locale : Locale.getISOCountries()) {
            locales.add(new Locale("", locale));
        }
        model.put("countries", locales);
        return "settings";
    }

    @RequestMapping(value = "/user/settings", method = RequestMethod.POST)
    @ResponseBody
    public String setSettings(
            HttpSession session,
            HttpServletRequest request,
            Map<String, Object> model
    ) {
        User user = (User) session.getAttribute("user");
        if (user == null) {
            return "{\"error\" : true, \"errorMessage\" : \"session is not valid\"}";
        }
//        @TODO set user's birthday
//        @TODO check all forms for empty fields
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String gender = request.getParameter("gender");
//        Date birthday = new Date(Integer.parseInt(request.getParameter("date")));
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String uri = request.getParameter("uri");
        String email = request.getParameter("email");
        System.out.println(email);
        AjaxResponse ajaxResponse = new AjaxResponse(false);
        if (! formValidator.validateEmail(email)) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage(formValidator.getErroressage());
        } else if (userService.getUserByEmail(email) != null && ! email.equals(user.getEmail())) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Email already in use");
        } else if (userService.getUserByUri(uri) != null && ! uri.equals(user.getUri())) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Uri already in use");
        } else {
            ajaxResponse.setRedirectType(true);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            if (gender != null) {
                user.setGender(gender);
            }
//            user.setBirthday(birthday);
            user.setCountry(country);
            user.setCity(city);
            user.setUri(uri);
            user.setEmail(email);
            userService.saveUser(user);
            ajaxResponse.setRedirectUrl("/user/settings");
        }
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/settings/password", method = RequestMethod.GET)
    public String changePassword(
            HttpSession session,
            Map<String, ObjectMapper> model
    ) {
        if (! RegisterLogIn.loggedIn(session)) {
            return "redirect:/";
        }
        return "changePassword";
    }

    @RequestMapping(value = "/user/settings/password", method = RequestMethod.POST)
    @ResponseBody
    public String setPassword(
            HttpServletRequest request,
            HttpSession session, Map<String,
            ObjectMapper> model
    ) {
        if (! RegisterLogIn.loggedIn(session)) {
            return "{\"error\" : true, \"errorMessage\" : \"session is not valid\"}";
        }
        User user = (User) session.getAttribute("user");
        AjaxResponse ajaxResponse = new AjaxResponse(false);
        String newPass = request.getParameter("newPassword");
        String repeatPass = request.getParameter("repeatPassword");
        if (! user.getPassword().equals(request.getParameter("oldPassword"))) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Old password is wrong");
        } else if (! formValidator.validatePassConfirm(newPass, repeatPass)) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage(formValidator.getErroressage());
        } else {
            user.setPassword(repeatPass);
            userService.saveUser(user);
            ajaxResponse.setRedirectType(true);
            ajaxResponse.setRedirectUrl("/user/settings");
        }
        try {
            return ow.writeValueAsString(ajaxResponse);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }
}
