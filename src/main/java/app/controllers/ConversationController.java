package app.controllers;

import app.entity.Conversation;
import app.entity.Message;
import app.entity.User;
import app.services.ConversationService;
import app.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by victor on 09/04/15.
 */
@Controller
public class ConversationController extends HttpServlet {
    @Autowired
    ConversationService conversationService;
    @Autowired
    MessageService messageService;

    @RequestMapping(value = "/conversation/{convUri}")
    public String connectcion(
            HttpSession session,
            @PathVariable String convUri,
            Map<String, Object> model
    ) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        Integer currentUserId =((User) session.getAttribute("user")).getId();
        Conversation conv = conversationService.checkConversationMember(convUri, currentUserId);
        if (conv == null) {
            model.put("title", "Access error!");
            model.put("errorMessage", "You do not have permission viewing this page");
            return "customError";
        }
        model.put("recipientId",
                conv.getCreatorId() == currentUserId ?
                        conv.getMemberId() : currentUserId);
        model.put("conversation", conv);
        model.put("messages", messageService.getMessagesInConv(conv.getId()));
        return "conversation";
    }

    @RequestMapping(value = "/conversation-settings/set-topic", method = RequestMethod.POST)
    @ResponseBody
    public void setTopic(
            HttpServletRequest request
    ) {
        String topic = request.getParameter("topic");
        if (! request.getParameter("topic").equals("")) {
            conversationService.setTopic(Integer.parseInt(request.getParameter("convId")), topic);
        }
    }

    @MessageMapping("/chat/conversation/{room}")
    public Message sendMessage(
            @DestinationVariable String room,
            Message message
    ) throws Exception {
        messageService.saveMessage(message);
        return message;
    }
}
