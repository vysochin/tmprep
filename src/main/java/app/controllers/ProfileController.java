/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.ajax.AjaxWallPost;
import app.util.Filler;
import app.wrappers.ProfileInfoWrapper;
import app.dao.UserDao;
import app.dao.WallPostDao;
import app.entity.User;
import app.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class ProfileController extends HttpServlet {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;

    @Autowired
    private WallPostDao wallPostDao;

    @Autowired
    private Filler filler;



    @RequestMapping(value = "/user/profile/{userUri}", method = RequestMethod.GET)
    public String profile(
            @PathVariable("userUri") String userUri,
            HttpServletRequest request,
            Map<String, Object> model,
            HttpSession session) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }

        model.put("title", "Profile");
        User user = (User) request.getSession().getAttribute("user");
        User currentUser = userService.getInitializedUserById(userDao.getUserByUri(userUri).getId());
        user = userService.getInitializedUserById(user.getId());

        ProfileInfoWrapper profileInfoWrapper = new ProfileInfoWrapper(user, currentUser);
        filler.fillProfileInfoWrapper(profileInfoWrapper);

        model.put("wrapper", profileInfoWrapper);
        model.put("wallList", filler.getWallPostInfoWrapperList(currentUser));
        return "profile";
    }

    @RequestMapping(value = "/user/profile/addWallPost", method = RequestMethod.POST)
    @ResponseBody
    public String addWallPost(HttpSession session,
                              HttpServletRequest request) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        String text = request.getParameter("text");
        int toUserId = Integer.parseInt(request.getParameter("toUserId"));
        AjaxWallPost ajaxWallPost = filler.getAjaxWallPostById(wallPostDao.addWallPost(session, toUserId , text));
        try {
            return ow.writeValueAsString(ajaxWallPost);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }

    @RequestMapping(value = "/user/profile/deleteWallPost", method = RequestMethod.POST)
    @ResponseBody
    public String deleteWallPost(HttpSession session, HttpServletRequest request) {
        if (! RegisterLogInController.loggedIn(session)) {
            return "redirect:/";
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        int wallPostId = Integer.parseInt(request.getParameter("wallPostId"));

        wallPostDao.deleteWallPost(wallPostDao.getWallPostById(wallPostId));
        AjaxWallPost ajaxWallPost = new AjaxWallPost();
        ajaxWallPost.setId(wallPostId);
        try {
            return ow.writeValueAsString(ajaxWallPost);
        } catch (Exception e) {
            return "{\"error\" : true, \"errorMessage\" : \"server error ...\"}";
        }
    }
}
