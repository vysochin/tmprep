/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.CountryService;
import app.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
public class Search extends HttpServlet {

    @Autowired
    UserService userService;
    @Autowired
    CountryService countryService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(
        HttpSession session,
        @RequestParam String nameCriteria,
        @RequestParam String countryCriteria,
        @RequestParam String genderCriteria,
        Map<String, Object> model
    ) {

        if (! RegisterLogIn.loggedIn(session)) {
            return "redirect:/";
        }
        List<Locale> locales = new ArrayList<>();
        for (String locale : Locale.getISOCountries()) {
            locales.add(new Locale("", locale));
        }
        model.put("title", "Search");
        model.put("countries", locales);
        if (nameCriteria.isEmpty() && countryCriteria.isEmpty() && genderCriteria.isEmpty()) {
            model.put("users", userService.getUsers());
        } else{
            model.put("users", userService.getUsersByCriteria(nameCriteria, countryCriteria, genderCriteria));
        }
        return "search";
    }
}
