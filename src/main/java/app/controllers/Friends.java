/**
 * Created by victor on 01/03/15.
 */

package app.controllers;

import app.Updater;
import app.UserService;
import app.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

//@Transactional
@Controller
public class Friends extends HttpServlet {

    @Autowired
    UserService userService;

    @Autowired
    SessionFactory sessionFactory;

    @Autowired
    Updater updater;

    @RequestMapping(value = "/user/friends", method = RequestMethod.GET)
    public String friends(HttpSession session, Map<String, Object> model) {
        if (! RegisterLogIn.loggedIn(session)) {
            return "redirect:/";
        }
        updater.updateDataOnClient(session);
        User user = (User) session.getAttribute("user");
        model.put("title", "Friends");
        model.put("friends", user.getFriends());

        return "friends";
    }

    @RequestMapping(value = "/user/friend/add/*", method = RequestMethod.GET)
    public String addFriend(HttpServletRequest request, HttpSession session, Map<String, Object> model) {
        if (! RegisterLogIn.loggedIn(session)) {
            return "redirect:/";
        }

        updater.updateDataOnClient(session);



        String newFriendUri = request.getServletPath().replace("/user/friend/add/", "");
        User user = (User) session.getAttribute("user");

        User mainUser = userService.getUserByUri(user.getUri());
        User newFriend = userService.getUserByUri(newFriendUri);

        if(!mainUser.containsFriend(newFriend)) {
            mainUser.addFriend(newFriend);
            newFriend.addFriend(mainUser);
            userService.saveUser(mainUser);
            userService.saveUser(newFriend);
            session.setAttribute("user", userService.getUserByUri(user.getUri()));
        }

        return "redirect:/user/profile/" + newFriendUri;
    }

    @RequestMapping(value = "/user/friend/delete/*", method = RequestMethod.GET)
    public String deleteFriend(HttpServletRequest request, HttpSession session, Map<String, Object> model) {
        if (! RegisterLogIn.loggedIn(session)) {
            return "redirect:/";
        }
        updater.updateDataOnClient(session);


        String newFriendUri = request.getServletPath().replace("/user/friend/delete/", "");
        User user = (User) session.getAttribute("user");
        User mainUser = userService.getUserByEmail(user.getEmail());
        User newFriend = userService.getUserByUri(newFriendUri);

        if(mainUser.containsFriend(newFriend)) {
            mainUser.deleteFriend(newFriend);
            newFriend.deleteFriend(mainUser);
            userService.saveUser(mainUser);
            userService.saveUser(newFriend);
            session.setAttribute("user", userService.getUserByUri(user.getUri()));
        }
        return "redirect:/user/profile/" + newFriendUri;
    }
}
