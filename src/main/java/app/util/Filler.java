package app.util;

import app.ajax.AjaxWallPost;
import app.dao.UserDao;
import app.dao.WallPostDao;
import app.entity.User;
import app.entity.WallPost;
import app.wrappers.ProfileInfoWrapper;
import app.wrappers.WallPostInfoWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by admin on 07.04.2015.
 */
@Repository
@Transactional
public class Filler {

    @Autowired
    private WallPostDao wallPostDao;

    @Autowired
    private UserDao userDao;

    public List<WallPostInfoWrapper> getWallPostInfoWrapperList(User user){
        List<WallPostInfoWrapper> wallPostInfoWrapperList = new ArrayList<>();

        userDao.getUserById(1);
        for(Object wallPost: wallPostDao.getWallPostListByUser(user)){
            WallPostInfoWrapper wallPostInfoWrapper = new WallPostInfoWrapper();
            wallPostInfoWrapper.setWallPost(((WallPost)wallPost));
            wallPostInfoWrapper.setAuthor(userDao.getUserById(((WallPost) wallPost).getFromUserId()));
            wallPostInfoWrapper.setRecipient(userDao.getUserById(((WallPost)wallPost).getToUserId()));
            wallPostInfoWrapperList.add(wallPostInfoWrapper);
        }
        Collections.sort(wallPostInfoWrapperList);
        Collections.reverse(wallPostInfoWrapperList);
        return wallPostInfoWrapperList;
    }

    public  void fillProfileInfoWrapper(ProfileInfoWrapper profileInfoWrapper){
        User loginUser = profileInfoWrapper.getLogInUser();
        User currentUser = profileInfoWrapper.getCurrentUser();
        if (!loginUser.equals(currentUser)) {
            profileInfoWrapper.setIsMe(false);
            if (loginUser.getFriends().contains(currentUser) &&
                    currentUser.containsFriend(loginUser)) {
                profileInfoWrapper.setIsMyFriend(true);
            } else {
                if (loginUser.getFriends().contains(currentUser)){
                    profileInfoWrapper.setIsNotConfirmedFriend(true);
                } else {
                    if (currentUser.containsFriend(loginUser)) {
                        profileInfoWrapper.setIsNotConfirmedMyRequest(true);
                    }
                }
            }
            if (loginUser.getBlackList().contains(currentUser)) {
                profileInfoWrapper.setIsCurrentUserBlackListed(true);
            }
            if (currentUser.getBlackList().contains(loginUser)) {
                profileInfoWrapper.setIsUserBlackListed(true);
            }
        }
    }

    public AjaxWallPost getAjaxWallPostById(int id){

        AjaxWallPost ajaxWallPost = new AjaxWallPost();
        WallPost wallPost = wallPostDao.getWallPostById(id);

        ajaxWallPost.setText(wallPost.getText());
        User user = userDao.getUserById(( wallPost).getFromUserId());
        ajaxWallPost.setFirstNameRecipient(user.getFirstName());
        ajaxWallPost.setLastNameRecipient(user.getLastName());
        if(user.getAvatarName() != null) {
            ajaxWallPost.setAvatarNameRecipient(userDao.getUserById((wallPost).getFromUserId()).getAvatarName());
        }else {
            ajaxWallPost.setAvatarNameRecipient("noAvatar.png");
        }
        ajaxWallPost.setDate(wallPost.getDate());
        ajaxWallPost.setId(wallPost.getId());
        ajaxWallPost.setUriRecipient(user.getUri());

        return ajaxWallPost;
    }
}
