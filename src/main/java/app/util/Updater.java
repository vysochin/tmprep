package app.util;

import app.dao.UserDao;
import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;

/**
 * Created by admin on 21.03.2015.
 */

@Repository
public class Updater {
    @Autowired
    private UserDao userDao;

    public void updateDataOnClient(HttpSession session){
        session.setAttribute("user", userDao.getUserByUri(((User) session.getAttribute("user")).getUri()));
    }
}
