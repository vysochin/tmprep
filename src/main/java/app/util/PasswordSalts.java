package app.util;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Created by victor on 12/04/15.
 */
public class PasswordSalts {
    public static final String ALGORITHM = "PBKDF2WithHmacSHA1";
    public static final int SALT_LENGTH = 16;
    public static final int ITERATION_COUNT = 8192;
    public static final int KEY_SIZE = 160;

    public static byte[] nextSalt() {
        byte[] salt = new byte[SALT_LENGTH];
        SecureRandom sr = new SecureRandom();
        sr.nextBytes(salt);
        return salt;
    }
    public static byte[] hashPassword(char[] password, byte[] salt)
            throws GeneralSecurityException {
        return hashPassword(password, salt, ITERATION_COUNT, KEY_SIZE);
    }

    public static byte[] hashPassword(char[] password, byte[] salt,
                                      int iterationCount, int keySize) throws GeneralSecurityException {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterationCount, keySize);
        SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
        return factory.generateSecret(spec).getEncoded();
    }
    public static boolean matches(char[] password, byte[] passwordHash,
                                  byte[] salt) throws GeneralSecurityException {
        return matches(password, passwordHash, salt, ITERATION_COUNT, KEY_SIZE);
    }

    public static boolean matches(char[] password, byte[] passwordHash,
                                  byte[] salt, int iterationCount, int keySize)
            throws GeneralSecurityException {
        return Arrays.equals(passwordHash, hashPassword(password, salt,
                iterationCount, keySize));
    }
}