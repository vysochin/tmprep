package app;

import app.entity.Country;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by admin on 12.03.2015.
 */

@Repository
@Transactional
public class CountryService {

    @Autowired
    SessionFactory sessionFactory;

    public List getCountry() {

        return sessionFactory.getCurrentSession()
                .createCriteria(Country.class)
                .list();
    }
}
