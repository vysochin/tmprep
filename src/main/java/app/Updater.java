package app;

import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

/**
 * Created by admin on 21.03.2015.
 */

@Repository
@Transactional
public class Updater {
    @Autowired
    private UserService userService;

    public void updateDataOnClient(HttpSession session){
        //User user = ;
        //userService.getUserByUri(((User) session.getAttribute("user")).getUri());
        session.setAttribute("user", userService.getUserByUri(((User) session.getAttribute("user")).getUri()));
    }
}
