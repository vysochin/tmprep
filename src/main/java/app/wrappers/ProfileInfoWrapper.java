package app.wrappers;

import app.entity.User;

import java.util.List;

/**
 * Created by admin on 05.04.2015.
 */

public class ProfileInfoWrapper {

    private User currentUser;
    private User logInUser;
    private List wallPostInfoWrapperList;
    private Boolean isMe = true;
    private Boolean isMyFriend = false;
    private Boolean isCurrentUserBlackListed = false;
    private Boolean isUserBlackListed = false;
    private Boolean isNotConfirmedMyRequest = false;
    private Boolean isNotConfirmedFriend = false;

    public ProfileInfoWrapper (){}
    public ProfileInfoWrapper (User logInUser, User currentUser){
        this.logInUser = logInUser;
        this.currentUser = currentUser;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public User getLogInUser() {
        return logInUser;
    }

    public void setLogInUser(User logInUser) {
        this.logInUser = logInUser;
    }

    public List getWallPostInfoWrapperList() {
        return wallPostInfoWrapperList;
    }

    public void setWallPostInfoWrapperList(List wallPostInfoWrapperList) {
        this.wallPostInfoWrapperList = wallPostInfoWrapperList;
    }

    public Boolean getIsMe() {
        return isMe;
    }

    public void setIsMe(Boolean isMe) {
        this.isMe = isMe;
    }

    public Boolean getIsMyFriend() {
        return isMyFriend;
    }

    public void setIsMyFriend(Boolean isMyFriend) {
        this.isMyFriend = isMyFriend;
    }

    public Boolean getIsCurrentUserBlackListed() {
        return isCurrentUserBlackListed;
    }

    public void setIsCurrentUserBlackListed(Boolean isCurrentUserBlackListed) {
        this.isCurrentUserBlackListed = isCurrentUserBlackListed;
    }

    public Boolean getIsUserBlackListed() {
        return isUserBlackListed;
    }

    public void setIsUserBlackListed(Boolean isUserBlackListed) {
        this.isUserBlackListed = isUserBlackListed;
    }

    public Boolean getIsNotConfirmedMyRequest() {
        return isNotConfirmedMyRequest;
    }

    public void setIsNotConfirmedMyRequest(Boolean isNotConfirmedMyRequest) {
        this.isNotConfirmedMyRequest = isNotConfirmedMyRequest;
    }

    public Boolean getIsNotConfirmedFriend() {
        return isNotConfirmedFriend;
    }

    public void setIsNotConfirmedFriend(Boolean isNotConfirmedFriend) {
        this.isNotConfirmedFriend = isNotConfirmedFriend;
    }
}