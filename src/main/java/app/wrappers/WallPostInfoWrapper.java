package app.wrappers;

import app.entity.User;
import app.entity.WallPost;

/**
 * Created by admin on 31.03.2015.
 */

public class WallPostInfoWrapper implements Comparable{


    private User recipient;
    private User author;
    private WallPost wallPost;

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public WallPost getWallPost() {
        return wallPost;
    }

    public void setWallPost(WallPost wallPost) {
        this.wallPost = wallPost;
    }

    @Override
    public int compareTo(Object o) {
        return this.getWallPost().getDate().compareTo(((WallPostInfoWrapper)o).getWallPost().getDate());
    }

}