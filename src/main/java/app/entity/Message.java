package app.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by victor on 28/03/15.
 */
@Entity
@Table(name = "Message")
public class Message {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "conv_id")
    private int conversationId;

    @Column(name = "author_id")
    private int authorId;

    @Column(name = "recipient_id")
    private int recipientId;

    @Column(name = "author_name")
    private String authorName;

    @Column(name = "dateSent")
    private Date dateSent;

    @Column(name = "messageBody")
    private String messageBody;

    public int getConversationId() {
        return conversationId;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(int recipientId) {
        this.recipientId = recipientId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

}
