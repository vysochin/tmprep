package app.entity;

import javax.persistence.*;

/**
 * Created by admin on 12.03.2015.
 */

@Entity
@Table(name = "Country")
public class Country {
    @Id
    @GeneratedValue
    @Column(name = "Id")
    private int id;

    @Column(name = "nameCountry")
    private String  nameCountry;

    public int getId() {
        return id;
    }

    public String getNameCountry() {
        return nameCountry;
    }
}
