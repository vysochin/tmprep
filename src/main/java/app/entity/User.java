package app.entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "User")
public class User {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

    @Column(name = "uri")
	private String uri;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName ;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private byte[] password;

	@Column(name = "salt")
	private byte[] salt;

	@Column(name = "gender")
	private String gender;

	@Column(name = "country")
	private String country;

	@Column(name = "city")
	private String city;

	@Column(name = "birthday")
	private Date birthday;

	@Column(name = "avatar_name")
	private String avatarName;

	@LazyCollection(LazyCollectionOption.EXTRA)
	@ManyToMany(cascade={CascadeType.ALL}, fetch = FetchType.LAZY)
	@JoinTable(name="friends",
			joinColumns={@JoinColumn(name="from_user_id")},
			inverseJoinColumns={@JoinColumn(name="to_user_id")})
	private List<User> friends;

	@ManyToMany(cascade={CascadeType.ALL}, fetch = FetchType.LAZY)
	@JoinTable(name="blacklist",
			joinColumns={@JoinColumn(name="from_user_id")},
			inverseJoinColumns={@JoinColumn(name="to_user_id")})
	private List<User> blackList;

    public User() {
        this.setUri("user" + UUID.randomUUID().getLeastSignificantBits());
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public byte[] getPassword() {
		return password;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public List<User> getBlackList() {
		return blackList;
	}

	public void setFriends(List<User> friends) {
		this.friends = friends;
	}

	public void setBlackList(List<User> blackList) {
		this.blackList = blackList;
	}

	public boolean containsInBlackList(User user){
		return blackList.contains(user);
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gander) {
		this.gender = gander;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void addFriend(User friend){
		if (!friends.contains(friend)) {
			friends.add(friend);
		}
	}

	public void deleteFriend(User friend){
		if (friends.contains(friend)) {
			friends.remove(friend);
		}
	}

	public List<User> getFriends() {
		return friends;
	}

	public boolean containsFriend(User user){
		return friends.contains(user);
	}


	public void addUserInBlackList(User user){
		if (!blackList.contains(user)) {
			blackList.add(user);
		}
	}

	public void removeUserFromBlackList(User user){
		if (blackList.contains(user)){
			blackList.remove(user);
		}
	}

	public boolean containsUserInBlackList(User user){
		return getBlackList().contains(user);
	}

	public String getAvatarName() {
		return avatarName;
	}

	public void setAvatarName(String avatarName) {
		this.avatarName = avatarName;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this)
			return true;

		if(obj == null)
			return false;


		if(!(getClass() == obj.getClass()))
			return false;
		else
		{
			User tmpUser = (User)obj;
			return tmpUser.getId() == this.getId();
		}
	}

}

