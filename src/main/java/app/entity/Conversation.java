package app.entity;

import javax.persistence.*;
import java.util.UUID;

/**
 * Created by victor on 09/04/15.
 */
@Entity
@Table(name = "Conversation")
public class Conversation {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "creator_id")
    private int creatorId;

    @Column(name = "creator_name")
    private String creatorName;

    @Column(name = "member_id")
    private int memberId;

    @Column(name = "member_name")
    private String memberName;

    @Column(name = "topic")
    private String topic;

    @Column(name = "uri")
    private String uri;

    public Conversation() {
        this.topic = "Conversation topic";
        this.uri = "conv" + UUID.randomUUID().getLeastSignificantBits();
    }

    public int getId() {
        return id;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int getMemberId() {
        return memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
