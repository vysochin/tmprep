package app.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by admin on 30.03.2015.
 */
@Entity
@Table(name = "wall_post")
public class WallPost {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "from_user_id")
    private int fromUserId;

    @Column(name = "to_user_id")
    private int toUserId;

    @Column(name = "text")
    private String text;

    @Column(name="date")
    private Date date;

    //@LazyCollection(LazyCollectionOption.FALSE)
    /*@ManyToOne(cascade={CascadeType.ALL})
    //@JoinColumn(name = "user_id") ///////////// its must be
    private User user;

    /*@ManyToOne(cascade={CascadeType.ALL})
    @JoinTable(name="post_user")
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade={CascadeType.ALL})
    @JoinTable(name="post_sent_user")
    @JoinColumn(name = "sent_user_id")
    private User sentUser;*/

    public WallPost(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(int fromUserId) {
        this.fromUserId = fromUserId;
    }

    public int getToUserId() {
        return toUserId;
    }

    public void setToUserId(int toUserId) {
        this.toUserId = toUserId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
