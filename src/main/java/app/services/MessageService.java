package app.services;

import app.dao.MessageDao;
import app.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by victor on 28/03/15.
 */
@Repository
@Transactional
public class MessageService {

    @Autowired
    private MessageDao messageDao;

    public void saveMessage(Message message) {
        messageDao.saveMessage(message);
    }

    public List<Message> getMessagesInConv(Integer convId) {
        return messageDao.getMessagesInConv(convId);
    }
}
