/**
 * Created by victor on 11/04/15.
 */

package app.services;

import app.ajax.AjaxResponse;
import app.util.FormValidator;
import app.util.PasswordSalts;
import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Repository
@Transactional
public class SettingsService {

    @Autowired
    FormValidator formValidator;

    @Autowired
    UserService userService;

    public AjaxResponse setSettings(HttpServletRequest request,
            User user) {
        AjaxResponse ajaxResponse = new AjaxResponse(false);
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String gender = request.getParameter("gender");
        String birthday = request.getParameter("birthday");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (birthday.equals("")) {
            user.setBirthday(null);
        } else {
            try {
                Date date = sdf.parse(birthday);
                user.setBirthday(date);
            } catch (ParseException e)  {
                System.out.println("Couldn't parse date");
            }
        }
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String uri = request.getParameter("uri");
        String email = request.getParameter("email");
        if (!formValidator.validateEmail(email)) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage(formValidator.getErroressage());
        } else if (!email.equals(user.getEmail()) && userService.getUserByEmail(email) != null) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Email already in use");
        } else if (!uri.equals(user.getUri()) && userService.getUserByUri(uri) != null) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Uri already in use");
        } else if (uri.equals("")) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Uri couldn't be empty");
        } else {
            ajaxResponse.setRedirectType(true);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            if (gender != null) {
                user.setGender(gender);
            }
            user.setCountry(country);
            user.setCity(city);
            user.setUri(uri);
            user.setEmail(email);
            userService.saveUser(user);
            ajaxResponse.setRedirectType(false);
        }
        return ajaxResponse;
    }

    public AjaxResponse changePassword(
            HttpServletRequest request,
            User user) {
        AjaxResponse ajaxResponse = new AjaxResponse(false);
        String newPass = request.getParameter("newPassword");
        String repeatPass = request.getParameter("repeatPassword");
        if (!user.getPassword().equals(request.getParameter("oldPassword"))) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Old password is wrong");
        } else if (!formValidator.validatePassConfirm(newPass, repeatPass)) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage(formValidator.getErroressage());
        } else {
            byte[] salt = PasswordSalts.nextSalt();
            byte[] hash;
            try {
                hash = PasswordSalts.hashPassword(repeatPass.toCharArray(), salt);
            } catch (GeneralSecurityException e) {
                ajaxResponse.setError(true);
                ajaxResponse.setErrorMessage("Server error");
                return ajaxResponse;
            }
            user.setPassword(hash);
            userService.saveUser(user);
            ajaxResponse.setRedirectType(false);
            ajaxResponse.setRedirectUrl("/user/settings");
        }
        return ajaxResponse;
    }

}
