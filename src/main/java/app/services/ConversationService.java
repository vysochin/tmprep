package app.services;

import app.wrappers.ProfileInfoWrapper;
import app.dao.ConversationDao;
import app.entity.Conversation;
import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by victor on 09/04/15.
 */
@Repository
@Transactional
public class ConversationService {

    @Autowired
    ConversationDao conversationDao;

    @Autowired
    UserService userService;

    private void save(Conversation conversation) {
        conversationDao.save(conversation);
    }

    private void delete(Conversation conversation) {
        conversationDao.delete(conversation);
    }

    private Conversation getConversationById(int id) {
        return conversationDao.getConversationById(id);
    }

    private Conversation getConversationByUri(String uri) {
        return conversationDao.getConversationByUri(uri);
    }

    public Conversation createConversation(User memberAuth, User member) {
        ProfileInfoWrapper pIW = new ProfileInfoWrapper(memberAuth, member);
        User user = userService.getInitializedUserById(pIW.getLogInUser().getId());
        if (! (user.getFriends().contains(pIW.getCurrentUser()) &&
            pIW.getCurrentUser().containsFriend(pIW.getLogInUser()))) {
            return null;
        }
        Conversation conversation = conversationDao.getConversationByMembers(memberAuth, member);
        if (conversation != null) {
            return conversation;
        }
        conversation = new Conversation();
        conversation.setCreatorId(memberAuth.getId());
        conversation.setCreatorName(memberAuth.getFirstName());
        conversation.setMemberId(member.getId());
        conversation.setMemberName(member.getFirstName());
        conversationDao.save(conversation);
        return conversation;
    }

    public Conversation getConversationByMembers(User member1, User member2) {
        return conversationDao.getConversationByMembers(member1, member2);
    }

    public Conversation checkConversationMember(String convUri, Integer userId) {
        return conversationDao.checkConversationMember(convUri, userId);
    }

    public List<Conversation> getConversations(int id) {
        return conversationDao.getConversations(id);
    }

    public void setTopic(int convId, String topic) {
        Conversation conv = conversationDao.getConversationById(convId);
        conv.setTopic(topic);
        conversationDao.save(conv);
    }
}
