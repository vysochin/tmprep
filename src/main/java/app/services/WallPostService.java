package app.services;

import app.util.UnproxClass;
import app.dao.WallPostDao;
import app.entity.WallPost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Created by admin on 31.03.2015.
 */

@Repository
@Transactional
public class WallPostService {

    @Autowired
    WallPostDao wallPostDao;

    public WallPost getInitializedWallPostById(int id){
        WallPost wallPost = wallPostDao.getWallPostById(id);

        return UnproxClass.initializeAndUnproxy(wallPost);
    }
}
