package app.services;

import app.ajax.AjaxResponse;
import app.util.FormValidator;
import app.util.PasswordSalts;
import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.security.GeneralSecurityException;

/**
 * Created by victor on 12/04/15.
 */
@Repository
@Transactional
public class RegisterLoginService {
    @Autowired
    UserService userService;
    @Autowired
    FormValidator formValidator;

    public AjaxResponse regUser (HttpServletRequest request, HttpSession session) {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        String confirmPass = request.getParameter("confirmPass");
        AjaxResponse ajaxResponse = new AjaxResponse(false);
        if (name.equals("") || email.equals("") || pass.equals("")
                || confirmPass.equals("")) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Fields shouldn't be empty");
        } else if (! formValidator.validateEmail(email) || ! formValidator.validatePassConfirm(pass, confirmPass)) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage(formValidator.getErroressage());
        } else if (userService.getUserByEmail(email) != null) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Email already in use");
        } else {
            ajaxResponse.setRedirectType(true);
            byte[] salt = PasswordSalts.nextSalt();
            byte[] hash;
            try {
                hash = PasswordSalts.hashPassword(pass.toCharArray(), salt);
                User user = userService.createUser(email, hash, salt, name);
                session.setAttribute("user", user);
                ajaxResponse.setRedirectUrl("/user/settings");
            } catch (GeneralSecurityException e) {
                ajaxResponse.setError(true);
                ajaxResponse.setErrorMessage("server error ...");
            }
        }
        return ajaxResponse;
    }

    public AjaxResponse loginUser(HttpServletRequest request, HttpSession session) {
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");
        AjaxResponse ajaxResponse = new AjaxResponse(false);
        if (email.equals("") || pass.equals("")) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage("Fields shouldn't be empty");
        } else if (! formValidator.validateEmail(email)) {
            ajaxResponse.setError(true);
            ajaxResponse.setErrorMessage(formValidator.getErroressage());
        } else {
            ajaxResponse.setRedirectType(true);
            User user = userService.getUserByEmailAndPassword(email, pass);
            if (user == null) {
                ajaxResponse.setError(true);
                ajaxResponse.setErrorMessage("Wrong email or password");
            } else {
                session.setAttribute("user", user);
                ajaxResponse.setRedirectUrl("/user/profile/" + user.getUri());
            }
        }
        return ajaxResponse;
    }
}
