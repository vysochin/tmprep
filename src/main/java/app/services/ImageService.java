package app.services;

import app.dao.UserDao;
import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by admin on 25.03.2015.
 */
@Repository
@Transactional
public class ImageService {

    @Autowired
    private UserDao userDao;
    private @Value("${pathToProject}") String pathToProject;

    private static final String PATH_TO_RESOURCES_DIRECTORY = "\\target\\social-den-vik-1.0-SNAPSHOT\\resources\\images\\avatars\\";


    public void saveImage(MultipartFile image, HttpSession session) {

        System.out.println("!!!" + pathToProject);
        String imageExtension = getFileExtension(image.getOriginalFilename());
        User logInUser = (User) session.getAttribute("user");
        String avatarName = logInUser.getUri()
                + System.currentTimeMillis()
                + imageExtension;
        try {

            loadFile(image, avatarName, pathToProject + PATH_TO_RESOURCES_DIRECTORY);

        } catch (Exception e) {
            e.printStackTrace();
        }
        logInUser.setAvatarName(avatarName);
        userDao.saveUser(logInUser);

    }

    private void loadFile(MultipartFile file, String fileName, String path) throws IOException {
        byte[] bytes = file.getBytes();
        File newFile = new File(path + fileName);
        BufferedOutputStream stream = new BufferedOutputStream(
                new FileOutputStream(newFile));
        stream.write(bytes);
        stream.close();
    }

    public String getFileExtension(String fileName) {
        int lastDotIndex = fileName.lastIndexOf(".");
        return fileName.substring(lastDotIndex);
    }
}
