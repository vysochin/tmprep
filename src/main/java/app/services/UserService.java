package app.services;

import app.util.UnproxClass;
import app.dao.UserDao;
import app.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;


/**
 * Created by admin on 07.03.2015.
 */

@Repository
@Transactional
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    SessionFactory sessionFactory;

    public User createUser(String email, byte[] password, byte[] salt, String name) {
        return userDao.createUser(email, password, salt, name);
    }

    public void saveUser(User user) {
        userDao.saveUser(user);
    }

    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    public List getUsers() {

        return userDao.getUsers();
    }

    public User getInitializedUserById(int id){
        User user = userDao.getUserById(id);

        return UnproxClass.initializeAndUnproxy(user);
    }

    public void addFriend(HttpSession session, String friendURI){

        User user = (User) session.getAttribute("user");
        User mainUser = userDao.getUserByUri(user.getUri());
        User newFriend =  userDao.getUserByUri(friendURI);

        if(!newFriend.containsFriend(user)) {
            this.removeUserFromBlackList(session, friendURI);
            newFriend.addFriend(mainUser);
             userDao.saveUser(newFriend);
        }
    }

    public void confirmFriend(HttpSession session, String friendURI){

        User user = (User) session.getAttribute("user");
        User mainUser = userDao.getUserByUri(user.getUri());
        User newFriend = userDao.getUserByUri(friendURI);

        if(!newFriend.containsFriend(user)) {
            this.removeUserFromBlackList(session, friendURI);
            newFriend.addFriend(mainUser);
            userDao.saveUser(newFriend);
            session.setAttribute("user", mainUser);
        }

    }

    public void deleteFriend(HttpSession session, String friendURI){

        User user = (User) session.getAttribute("user");
        User mainUser = userDao.getUserByEmail(user.getEmail());
        User friend = userDao.getUserByUri(friendURI);

        if(mainUser.containsFriend(friend)) {
            mainUser.deleteFriend(friend);
            userDao.saveUser(mainUser);
            session.setAttribute("user", mainUser);
        }
        if(friend.containsFriend(mainUser)){
            friend.deleteFriend(mainUser);
            userDao.saveUser(friend);
        }
    }

    public void addUserInBlackList(HttpSession session, String badUserURI){

        User user = (User) session.getAttribute("user");
        User mainUser = userDao.getUserByUri(user.getUri());
        User badUser = userDao.getUserByUri(badUserURI);

        if(!mainUser.containsUserInBlackList(badUser)) {
            this.deleteFriend(session, badUserURI);
            mainUser.addUserInBlackList(badUser);
            userDao.saveUser(mainUser);
            session.setAttribute("user", mainUser);
        }
    }

    public void removeUserFromBlackList(HttpSession session, String badUserURI){

        User user = (User) session.getAttribute("user");
        User mainUser = userDao.getUserByUri(user.getUri());
        User badUser = userDao.getUserByUri(badUserURI);

        if( mainUser.containsUserInBlackList(badUser)) {
            mainUser.removeUserFromBlackList(badUser);
            userDao.saveUser(mainUser);
            session.setAttribute("user", mainUser);
        }
    }

    public User getUserByUri(String uri) {
        return userDao.getUserByUri(uri);
    }

    public User getUserByEmailAndPassword(String email, String password) {
        return userDao.getUserByEmailAndPassword(email, password);
    }

}