package app.ajax;

/**
 * Created by admin on 12.04.2015.
 */
public class AjaxProfile {

    private String name;
    private String uri;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
