package app.ajax;

import java.util.Date;

/**
 * Created by admin on 11.04.2015.
 */
public class AjaxWallPost {
    private String firstNameRecipient;
    private String lastNameRecipient;
    private String avatarNameRecipient;
    private String uriRecipient;
    private String text;
    private Date date;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstNameRecipient() {
        return firstNameRecipient;
    }

    public void setFirstNameRecipient(String firstNameRecipient) {
        this.firstNameRecipient = firstNameRecipient;
    }

    public String getLastNameRecipient() {
        return lastNameRecipient;
    }

    public void setLastNameRecipient(String lastNameRecipient) {
        this.lastNameRecipient = lastNameRecipient;
    }

    public String getAvatarNameRecipient() {
        return avatarNameRecipient;
    }

    public void setAvatarNameRecipient(String avatarNameRecipient) {
        this.avatarNameRecipient = avatarNameRecipient;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUriRecipient() {
        return uriRecipient;
    }

    public void setUriRecipient(String uriRecipient) {
        this.uriRecipient = uriRecipient;
    }
}
