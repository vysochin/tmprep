package app.ajax;

import java.util.HashMap;

/**
 * Created by victor on 08/03/15.
 */

public class AjaxResponse {
    private boolean error;
    private String errorMessage = "";
    private boolean redirectType = false;
    private String redirectUrl = "";
    private boolean fillForm = false;
    private HashMap<String, String> formFields;

    public AjaxResponse (boolean error) {
        this.error = error;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isRedirectType() {
        return redirectType;
    }

    public void setRedirectType(boolean redirectType) {
        this.redirectType = redirectType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void setFormField(String classname, String val) {
        formFields.put(classname, val);
    }

    public String getFormField(String classname) {
        return formFields.get(classname);
    }

    public boolean getFillForm() {
        return fillForm;
    }

    public void setFillForm(boolean val) {
        fillForm = val;
    }
}
